﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {

            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "http://api.ispras.ru/texterra/v1/nlp?targetType=lemma&apikey=key");
            request.Content = new StringContent("[{\"text\":\" 123   \"}]",
                                                Encoding.UTF8,
                                                "application/json");//CONTENT-TYPE header

            var x = client.SendAsync(request).Result;


            Console.WriteLine("Response: {0}", x.Content.ReadAsStringAsync().Result);
        }
    }
}
